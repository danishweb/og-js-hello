FROM node:alpine

# App
COPY . /web
WORKDIR /web
# Install app dependencies
RUN npm install

EXPOSE  8080
ENTRYPOINT ["node", "./index.js"]
