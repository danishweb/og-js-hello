GPROJECT_ID = origin-demo-212611
IMAGE = og-node-back
PROJECT = og-node-bask

cloud/init:
	gcloud config set project  $(GPROJECT_ID)
	gcloud config set compute/zone australia-southeast1
	gcloud auth configure-docker
	gcloud container clusters get-credentials cluster-1 --zone=australia-southeast1
build/local:
	docker build -t $(IMAGE) .
	docker tag $(IMAGE) gcr.io/$(GPROJECT_ID)/$(IMAGE):v2
build/gcloud:
	gcloud builds submit --config cloudbuild.yaml .
build/glcoud-cli:
	gcloud builds submit --tag gcr.io/$(GPROJECT_ID)/$(IMAGE) .
push/manual:
	docker push gcr.io/$(GPROJECT_ID)/$(IMAGE):v2
create/deployment:
	kubectl run $(PROJECT) --image=gcr.io/$(GPROJECT_ID)/$(IMAGE):v2 --port 8080
deploy/gcloud:
	kubectl set image deployment/$(PROJECT) $(PROJECT)=gcr.io/$(GPROJECT_ID)/$(IMAGE):v2
